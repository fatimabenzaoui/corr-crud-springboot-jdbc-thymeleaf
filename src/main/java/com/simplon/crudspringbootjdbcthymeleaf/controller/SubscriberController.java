package com.simplon.crudspringbootjdbcthymeleaf.controller;

import com.simplon.crudspringbootjdbcthymeleaf.dao.SubscriberDAO;
import com.simplon.crudspringbootjdbcthymeleaf.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class SubscriberController {
    
    // INJECT SubscriberDAO
    @Autowired
    private SubscriberDAO subscriberDAO;


    /**
     * renvoie la page index.html
     * renvoie la liste des abonnés
     * @param model
     * @return
     */
    @GetMapping("/")
    public String showIndexPageAndGetSubscribersList(Model model) {
        model.addAttribute("subscribers", subscriberDAO.findAll());
        return "index";
    }

    /**
     * renvoie la page add.html
     * renvoie un subscriber
     * @param model
     * @return
     */
    @GetMapping("/new")
    public String getAddPage(Model model) {
        Subscriber subscriber = new Subscriber();
        model.addAttribute("subscriber", subscriber);
        return "add";
    }

    /**
     * ajoute un abonné
     * redirige vers la liste des abonnés
     * @param subscriber
     * @param model
     * @return
     */
    @PostMapping("/add")
    public String addSubscriber(@ModelAttribute("subscriber") Subscriber subscriber, Model model) {
        subscriberDAO.add(subscriber);
        return "redirect:/";
    }

    /**
     * renvoie la page get.html (tableau avec détails de l'abonné)
     * renvoie un abonné avec son id
     * renvoie un objet mav de type ModelAndView
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
	public ModelAndView showGetPageAndGetSubscriberById(@PathVariable(name = "id") int id) {
	    ModelAndView mav = new ModelAndView("get");
	    Subscriber subscriber = subscriberDAO.findById(id);
	    mav.addObject("subscriber", subscriber);
	    return mav;
	}


    /**
     * renvoie la page edit.html
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    public ModelAndView showEditForm(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit");
        Subscriber subscriber = subscriberDAO.findById(id);
        mav.addObject("subscriber", subscriber);
        return mav;
    }

    /**
     * modifie un abonné
     * @param subscriber
     * @return
     */
    @PostMapping("/update/{id}")
    public String updateSubscriberById(@ModelAttribute("subscriber") Subscriber subscriber, @PathVariable(name = "id") int id) {
        subscriber.setSubscriberId(id);
        subscriberDAO.updateById(subscriber.getFirstName(), subscriber.getLastName(), subscriber.getEmail(), subscriber.getSubscriberId());
        return "redirect:/";
    }


    /**
     * supprime un abonné avec son id
     * @param id
     * @return
     */
    @GetMapping(value = "/delete/{id}", produces = "application/json")
    public String deleteSubscriberById(@PathVariable(name = "id") int id) {
	    subscriberDAO.deleteById(id);
	    return "redirect:/";       
	} 
}
