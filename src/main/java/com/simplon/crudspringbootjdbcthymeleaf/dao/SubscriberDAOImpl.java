package com.simplon.crudspringbootjdbcthymeleaf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.simplon.crudspringbootjdbcthymeleaf.model.Subscriber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    // INJECT DATASOURCE
    @Autowired
    private DataSource dataSource;

    // REQUETES SQL
    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";
    private static final String ADD_SUBSCRIBER = "INSERT INTO subscribers (firstName, lastName, email) VALUES (?,?,?)";
    private static final String UPDATE_SUBSCRIBER_BY_ID = "UPDATE subscribers SET firstName = ?, lastName = ?, email = ? WHERE subscriberId = ?";
    private static final String DELETE_SUBSCRIBER_BY_ID = "DELETE FROM subscribers WHERE subscriberId = ?";
    
    /**
     * retourne la liste des abonnés
     * @return
     */
    @Override
    public List<Subscriber> findAll() {
        Connection cnx = null;
        List<Subscriber> list = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Subscriber(
                    rs.getInt("subscriberId"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("email"),
                    rs.getTimestamp("createdAt")));
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return list;
    }

    /**
     * ajoute un abonné
     * @param subscriber
     */
    @Override
    public Subscriber add(Subscriber subscriber) {
        Connection cnx;
            try {
                cnx = dataSource.getConnection();
                PreparedStatement ps = cnx.prepareStatement("INSERT INTO subscribers (firstName, lastName, email) VALUES (?, ?, ?)");
                ps.setString(1, subscriber.getFirstName());
                ps.setString(2, subscriber.getLastName());
                ps.setString(3, subscriber.getEmail());
                ps.executeUpdate();
                cnx.close();
            
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return subscriber;            
    }

    @Override
    public Subscriber findById(int id) {
        Connection cnx;
        Subscriber subscriber = null;

        try {

            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement("SELECT * FROM subscribers WHERE subscriberId = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return new Subscriber(
                    rs.getInt("subscriberId"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("email"),
                    rs.getTimestamp("createdAt")
                );

            }
        cnx.close();   
        } catch (Exception e) {
            e.getMessage();
        }
        
        return subscriber;
    }

    @Override
    public void updateById(String newFname, String newLname, String newEmail, Integer subscriberId) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(UPDATE_SUBSCRIBER_BY_ID);
            ps.setString(1, newFname);
            ps.setString(2, newLname);
            ps.setString(3, newEmail);
            ps.setInt(4, subscriberId);
            ps.executeUpdate();
            cnx.close();
        } catch (Exception e) {
            e.getMessage();
        }
        
    }

    /**
     * supprime un abonné
     */
    @Override
    public void deleteById(int id) {
        Connection cnx;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(DELETE_SUBSCRIBER_BY_ID);
            ps.setInt(1, id);
            int executeUpdate = ps.executeUpdate();
            if (executeUpdate == 1) {
                System.out.println("Subscriber is deleted with ID : " + id);
            }
            cnx.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    

    

}