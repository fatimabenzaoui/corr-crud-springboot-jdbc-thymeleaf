package com.simplon.crudspringbootjdbcthymeleaf.dao;

import java.util.List;

import com.simplon.crudspringbootjdbcthymeleaf.model.Subscriber;

public interface SubscriberDAO {
    
    public List<Subscriber> findAll();

    public Subscriber add(Subscriber subscriber);

    public Subscriber findById(int id);

    public void updateById(String newFname, String newLname, String newEmail, Integer subscriberId);

    public void deleteById(int id);
    
}
