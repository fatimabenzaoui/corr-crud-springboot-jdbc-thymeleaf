package com.simplon.crudspringbootjdbcthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudSpringbootJdbcThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringbootJdbcThymeleafApplication.class, args);
	}

}
