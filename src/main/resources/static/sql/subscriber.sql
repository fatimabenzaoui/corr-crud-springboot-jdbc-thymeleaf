-- CREATE DATABASE
CREATE DATABASE IF NOT EXISTS `springbootjdbcthymeleaf`;
USE `springbootjdbcthymeleaf`;

-- CREATE TABLE
DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE subscribers (
    subscriberId INTEGER PRIMARY KEY AUTO_INCREMENT,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE KEY,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- INSERT DATA
INSERT INTO `subscribers`(`firstName`, `lastName`, `email`) 
VALUES ("Pierre","Dupont","pierredupont@gmail.com");