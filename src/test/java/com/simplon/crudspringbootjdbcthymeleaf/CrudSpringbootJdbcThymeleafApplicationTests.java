package com.simplon.crudspringbootjdbcthymeleaf;

import java.util.List;

import com.simplon.crudspringbootjdbcthymeleaf.dao.SubscriberDAO;
import com.simplon.crudspringbootjdbcthymeleaf.model.Subscriber;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
class CrudSpringbootJdbcThymeleafApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private SubscriberDAO subscriberDAO;

	// Teste findAll()
	@Test
	public void testFindAll() {

		List<Subscriber> subscribers = subscriberDAO.findAll();
		for (Subscriber subscriber:subscribers){
			System.out.println(
                "*********************\n\n" + 
                "@TEST : FIND ALL\n\n" + 
                subscriber + 
                "\n\n*********************"
            );
		}
	}

	// Teste add()
	@Test
	public void testAdd() {
		Subscriber subscriber = new Subscriber("Hugo", "Parmentier", "hugo@mail.com");
		subscriberDAO.add(subscriber);
	}

	// Teste findById()
	@Test
	public void testFindById() {
		Subscriber subscriber = subscriberDAO.findById(77);
		System.out.println(
            "*********************\n\n" + 
            "@TEST : FIND BY ID\n\n" + 
            subscriber + 
            "\n\n*********************"
        );
	}

	// Teste UpdateById()
	@Test
	public void testUpdateById() {
		Subscriber subscriber = subscriberDAO.findById(1);
		subscriber.setEmail("pierre@gmail.com");
		subscriberDAO.updateById("pierre", "dupont", "pierre@gmail.com", 1);
		System.out.println(
            "*********************\n\n" + 
            "@TEST : UPDATE BY ID\n\n" + 
            subscriber + 
            "\n\n*********************"
        );
	}

	// teste la méthode deleteById()
    @Test
    public void testDeleteById() {
        subscriberDAO.deleteById(77);
    }

}
